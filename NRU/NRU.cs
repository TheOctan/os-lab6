﻿using Lab6.Memory.Page;
using Lab6.Replacement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NRU
{
	// 1) Алгоритм NRU
	// 
	// NRU(Not Recently Used) удаляет произвольную страницу,
	// относящуюся к самому низкому непустому классу. В этот алгоритм заложена
	// идея, суть которой в том, что лучше удалить модифицированную страницу, к
	// которой не было обращений по крайней мере за последний такт системных
	// часов (обычно это время составляет около 20 мс), чем удалить интенсивно
	// используемую страницу. Главная привлекательность алгоритма NRU в том,
	// что его нетрудно понять, сравнительно просто реализовать и добиться от
	// него производительности, которая, конечно, не оптимальна, но может быть
	// вполне приемлема.

	public class NRU : IReplacer
	{
		public IModifiablePage Replace(List<IModifiablePage> list)
		{
			var replaceablePage = list.FirstOrDefault(x => x.IsModified && !x.IsReaded);

			if (replaceablePage != null)
			{
				list.Remove(replaceablePage);
			}
			else
			{
				replaceablePage = list.FirstOrDefault();
				list.Remove(replaceablePage);
			}

			return replaceablePage;
		}
	}
}
