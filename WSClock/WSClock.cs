﻿using Lab6;
using Lab6.Memory.Page;
using Lab6.Replacement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WSClock
{
	// 5) Алгоритм WSClock
	//
	// Усовершенствованный алгоритм, основанный на алгоритме «часы», но
	// также использующий информацию о рабочем наборе(набор страниц,
	// использующихся процессом в данный момент), называется WSClock(Carr
	// and Hennessey, 1981). Благодаря простоте реализации и хорошей
	// производительности он довольно широко используется на практике.
	// Необходимая структура данных сводится к циклическому списку страничных
	// блоков, как в алгоритме «часы». Изначально этот список пуст. При загрузке
	// первой страницы она добавляется к списку. По мере загрузки следующих
	// страниц они попадают в список, формируя замкнутое кольцо.
	// Как и в алгоритме «часы», при каждой ошибке отсутствия страницы
	// сначала проверяется страница, на которую указывает стрелка. Если бит R
	// установлен в 1, значит, страница была использована в течение текущего
	// такта, поэтому она не является идеальным кандидатом на удаление. Затем
	// бит R устанавливается в 0, стрелка перемещается на следующую страницу, и
	// алгоритм повторяется уже для нее.
	// Теперь посмотрим, что получится, если у страницы, на которую
	// указывает стрелка, бит R = 0. Если ее возраст превышает значение t и
	// страница не изменена, она не относится к рабочему набору и ее точная копия
	// присутствует на диске.Тогда страничный блок просто удаляется, и в него
	// помещается новая страница. С другой стороны, если страница была
	// изменена, ее блок не может быть тотчас же удалён, поскольку на диске нет ее
	// точной копии.Чтобы избежать переключения процесса, запись на диск
	// планируется, а стрелка перемещается дальше и алгоритм продолжает свою
	// работу на следующей странице. В конце концов должна попасться старая,
	// неизмененная страница, которой можно будет тут же и воспользоваться.
	// В принципе за один оборот часовой стрелки может быть запланирована
	// операция дискового ввода-вывода для всех страниц. Для уменьшения потока
	// обмена данными с диском может быть установлен лимит, позволяющий
	// сбрасывать на диск максимум n страниц.По достижении этого лимита новые
	// записи на диск уже не планируются.

	public class WSClock : IReplacer
	{
		private int count = 0;
		private int currentPage = 0;
		private int CurrentPage
		{
			get => currentPage;
			set => currentPage = value % count;
		}

		private readonly TimeSpan age;

		public WSClock(int countCicles)
		{
			age = TimeSpan.FromSeconds(Breaker.timeCicle.TotalSeconds * countCicles);
		}

		public IModifiablePage Replace(List<IModifiablePage> list)
		{
			count = list.Count();
			int wsClock = 0;

			IModifiablePage replaceablePage = null;

			while (!list[CurrentPage++].IsReaded)
			{
				if (wsClock++ >= count)
				{
					replaceablePage = list.FirstOrDefault();
					list.Remove(replaceablePage);
					return replaceablePage;
				}
			}

			if (list[CurrentPage].PageAge >= age)
			{
				replaceablePage = list[CurrentPage];
				
			}
			else
			{
				replaceablePage = replaceablePage = list.FirstOrDefault();
			}

			list.Remove(replaceablePage);
			return list.First();
		}
	}
}
