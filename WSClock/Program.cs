﻿using Lab6;
using Lab6.Log;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WSClock
{
    class Program
    {
        static void Main(string[] args)
        {
			Breaker breaker = new Breaker(new WSClock(2));
			breaker.SetLogger(new ConsoleLogger());

			try
			{
				breaker.Run();
			}
			catch (Exception e)
			{
				Console.WriteLine(e.Message);
			}
		}
    }
}
