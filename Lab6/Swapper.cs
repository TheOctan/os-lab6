﻿using Lab6.Memory.Page;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab6
{
	public class Swapper
	{
		public bool SavePage(string directory, IPage page)
		{
			using (FileStream file = new FileStream(directory + "/" + page.ID.ToString(), FileMode.Create, FileAccess.Write))
			{
				using (StreamWriter writer = new StreamWriter(file))
				{
					writer.WriteLine(page.Next);
				}
			}

			return true;
		}

		public MemoryPage LoadPage(string directory, Guid ID)
		{
			string fullPagePath = directory + "/" + ID.ToString();

			if (File.Exists(fullPagePath))
			{
				using (FileStream file = new FileStream(fullPagePath, FileMode.Open, FileAccess.Read))
				{
					using (StreamReader reader = new StreamReader(file))
					{
						var next = Guid.Parse(reader.ReadLine());

						return new MemoryPage(ID)
						{
							Next = next
						};
					}
				}
			}

			return null;
		}

		public List<IModifiablePage> LoadFirstPages(string directory, int count)
		{
			if (count <= 0)
				throw new ArgumentException("page count should be positive");

			int current = 1;

			if (Directory.Exists(directory))
			{
				List<IModifiablePage> pages = new List<IModifiablePage>();

				foreach (var fileName in Directory.GetFiles(directory))
				{
					var ID = Guid.Parse(fileName.Remove(0, directory.Length + 1));
					var page = LoadPage(directory, ID);

					if (page != null)
						pages.Add(page);

					if (++current > count)
						break;
				}

				return pages;
			}

			return null;
		}

		public List<IModifiablePage> LoadAllPages(string directory)
		{
			if (Directory.Exists(directory))
			{
				List<IModifiablePage> pages = new List<IModifiablePage>();

				foreach (var fileName in Directory.GetFiles(directory))
				{
					var ID = Guid.Parse(fileName.Remove(0, directory.Length + 1));
					var page = LoadPage(directory, ID);

					if (page != null)
						pages.Add(page);
				}

				return pages;
			}

			return null;
		}

		public int CountPagesOnDirectory(string directory)
		{
			if (Directory.Exists(directory))
			{
				return Directory.GetFiles(directory).Length;
			}

			throw new ArgumentException(string.Format("Directory \"{0}\" not found", directory));
		}
	}
}
