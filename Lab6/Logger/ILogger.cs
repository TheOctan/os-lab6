﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab6.Log
{
	public interface ILogger
	{
		void Log(string message);
	}
}
