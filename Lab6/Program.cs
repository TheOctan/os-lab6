﻿using Lab6.Log;
using Lab6.Memory;
using Lab6.Memory.Page;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Lab6
{
	class Program
	{
		public static Swapper swapper = new Swapper();

		static void Main(string[] args)
		{
			Console.Write("Input count pages: ");
			int count = int.Parse(Console.ReadLine());
			CreatePageCicle(count);

			//PageList memory = new PageList(3);

			//while (memory.AddPage(new MemoryPage(TimeSpan.FromSeconds(1))));

			//foreach (var item in memory)
			//{
			//	Console.WriteLine(item.ID);
			//	swapper.SavePage("pages", item);
			//}

			//swapper.SavePage("pages", new MemoryPage(TimeSpan.FromSeconds(1)));

			// loading concrtete page by identifier
			//var page = swapper.LoadPage("pages", Guid.Parse("52982902-3f1e-48c5-8bf4-4d6d6d07f969"));
			//var nextPage = swapper.LoadPage("pages", page.Next);
			//Console.WriteLine(nextPage);

			// loading all pages from file
			//foreach (var page in swapper.LoadAllPages("pages"))
			//{
			//	Console.WriteLine(page);
			//}

			// loading first pages from file
			//foreach (var page in swapper.LoadFirstPages("pages", 2))
			//{
			//	Console.WriteLine(page);
			//}

			// ...
			//Console.WriteLine(swapper.CountPagesOnDirectory("pages"));
		}

		public static void CreatePageCicle(int count)
		{
			PageList memory = new PageList(count);

			while (memory.AddPage(new MemoryPage()));

			var list = memory.Pages.OrderBy(x => x.ID.ToString()).ToList();

			for (int i = 0; i < list.Count - 1; i++)
			{
				var currentPage = list[i];
				var nextPage = list[i + 1];

				currentPage.Next = nextPage.ID;
			}

			list.Last().Next = list.First().ID;

			foreach (var item in memory)
			{
				swapper.SavePage("pages", item);
			}
		}
	}
}
