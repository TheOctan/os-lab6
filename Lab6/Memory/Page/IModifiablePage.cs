﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab6.Memory.Page
{
    public interface IModifiablePage : IReadablePage, IPage
    {
		new Guid Next { get; set; }
		bool IsModified { get; set; }
    }
}
