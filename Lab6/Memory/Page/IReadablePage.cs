﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab6.Memory.Page
{
    public interface IReadablePage : IPage
    {
        bool IsReaded { get; set; }
    }
}
