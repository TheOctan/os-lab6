﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab6.Memory.Page
{
	public class MemoryPage : IModifiablePage, IReadablePage, IEquatable<MemoryPage>
	{
		public Guid ID { get; }
		public Guid Next { get; set; }
		public TimeSpan PageAge { get; set; }

		// Бит R
		// устанавливается при каждом обращении к странице(при чтении или записи).
		// обозначает только чтение (read only)
		// периодически сбрасывается по прерыванию таймера
		public bool IsReaded { get; set; }

		// Бит М
		// устанавливается, когда в страницу ведется запись (то есть когда она модифицируется).
		// чтение-запись (read-write)
		public bool IsModified { get; set; }

		public MemoryPage() : this(Guid.NewGuid())
		{

		}
		public MemoryPage(Guid ID)
		{
			IsReaded = false;
			IsModified = false;
			this.ID = ID;
			Next = Guid.Empty;
		}

		public override string ToString()
		{
			return string.Format("ID: {0}\nNext Page: {1}", ID, Next);
		}

		public bool Equals(MemoryPage other)
		{
			return ID == other.ID;
		}

		// Класс 0: в последнее время не было ни обращений, ни модификаций.
		// Класс 1: обращений в последнее время не было, но страница модифицирована.
		// Класс 2: в последнее время были обращения, но модификаций не было.
		// Класс 3: в последнее время были и обращения и модификации.
	}
}
