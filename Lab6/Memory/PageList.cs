﻿using Lab6.Memory.Page;
using Lab6.Replacement;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab6.Memory
{
	public class PageList : IEnumerable<IModifiablePage>
	{
		public int Count => pages.Count;
		public int Capacity { get; }
        public IEnumerable<IModifiablePage> Pages => pages;

		public PageList(int size)
		{
			Capacity = size;
		}

		public bool AddPage(IModifiablePage page)
		{
			if (pages.Count < Capacity)
			{
				pages.Add(page);
				return true;
			}

			return false;
		}
        public IModifiablePage ReplacePage(IReplacer replacer)
        {
            return replacer.Replace(pages);
        }

        public IEnumerator<IModifiablePage> GetEnumerator()
		{
			return pages.GetEnumerator();
		}
		IEnumerator IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}

		private List<IModifiablePage> pages = new List<IModifiablePage>();
	}
}
