﻿using Lab6.Accessor;
using Lab6.Log;
using Lab6.Memory;
using Lab6.Memory.Page;
using Lab6.Replacement;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Lab6
{
	public class Breaker
	{
		public static readonly Stopwatch timer = new Stopwatch();
		public static readonly TimeSpan timeCicle = TimeSpan.FromSeconds(timeQuantum);

		private readonly Stopwatch breaker = new Stopwatch();

		private readonly IReplacer replacer;
		private ILogger logger;
		private Random random;

		private readonly PageAccessor accessor;
		private readonly Swapper swapper;
		private PageList list;
		private IModifiablePage currentPage;

		private const double timeQuantum = 0.5;
		private const int resetCycles = 2;
		private const int capacity = 4;
		private const string directory = "pages";

		public Breaker(IReplacer replacer)
		{
			this.replacer = replacer;
			random = new Random();

			accessor = new PageAccessor();
			list = new PageList(capacity);
			swapper = new Swapper();
		}

		public void SetLogger(ILogger logger)
		{
			this.logger = logger;
		}

		public int Run()
		{
			if (!Init())
			{
				return 1;
			}

			timer.Start();
			breaker.Start();
			logger?.Log("Running");

			while (true)
			{
				Step();
				if (CheckForRemovingBitR())
				{
					//logger?.Log("Reset bit R");
				}

				// Modifying and Reading of page
				if (ModifyCurrentPage())
				{
					logger?.Log(string.Format("Current page {0} modifided", currentPage.ID.ToString().Substring(0, 6)));
				}
				else if (accessor.ReadPage(currentPage))
				{
					logger?.Log(string.Format("Current page {0} readed", currentPage.ID.ToString().Substring(0, 6)));
				}

				// Searching next page
				var nextPage = FindNextPage(currentPage);
				if (nextPage != null)
				{
					currentPage = nextPage;
					continue;
				}
				else
				{
					var replacedPage = list.ReplacePage(replacer);
					if (replacedPage == null)
					{
						logger?.Log("There was no place to replace");
						break;
					}

					logger?.Log(string.Format("Next page {0} replaced", replacedPage.ID.ToString().Substring(0, 6)));

					if (replacedPage.IsModified)
					{
						swapper.SavePage(directory, replacedPage);
					}

					var loadedPage = swapper.LoadPage(directory, currentPage.Next);
					if (loadedPage != null)
					{
						if (list.AddPage(loadedPage))
						{
							currentPage = loadedPage;
							logger?.Log(string.Format("Loaded page {0} loaded", loadedPage.ID.ToString().Substring(0, 6)));
						}
					}
					else
					{
						logger?.Log("Next page not found");
						break;
					}
				}
			}

			logger?.Log("Stoping process");
			return 0;
		}

		private bool Init()
		{
			logger?.Log("Initialization");

			int countPagesDir;
			try
			{
				countPagesDir = swapper.CountPagesOnDirectory(directory);
			}
			catch (ArgumentException e)
			{
				logger?.Log("Invalid initialization: " + e.Message);
				return false;
			}

			if (countPagesDir == 0)
			{
				logger?.Log("Invalid initialization: Directory contains no pages");
				return false;
			}

			int listCapacity = list.Capacity;
			int countLoadedPages = countPagesDir > listCapacity ? listCapacity : countPagesDir;

			logger?.Log(string.Format("Count of pages in directory {0}", countPagesDir));
			logger?.Log(string.Format("Capacity of the list: {0}", listCapacity));
			logger?.Log(string.Format("Count of loaded pages {0}", countLoadedPages));
			var pages = swapper.LoadFirstPages(directory, countLoadedPages);

			if (pages != null)
			{
				foreach (var page in pages)
				{
					if (!list.AddPage(page))
						break;
				}
			}

			var startPage = list.Pages.FirstOrDefault();
			currentPage = startPage;

			logger?.Log("Pages loaded");

			return true;
		}

		private void Step()
		{

			Thread.Sleep((int)timeCicle.TotalMilliseconds);
			//Console.ReadKey();

			if (breaker.Elapsed >= timeCicle)
			{
				breaker.Restart();
			}
		}
		private void IncreasePageAge()
		{
			foreach (var page in list.Pages)
			{
				page.PageAge += timeCicle;
			}
		}
		private bool ResetBitR()
		{
			bool result = true;

			foreach (var page in list.Pages)
			{
				result = result && accessor.ResetBitR(page);
			}

			return result;
		}
		private bool CheckForRemovingBitR()
		{
			if (breaker.Elapsed.TotalSeconds >= timeCicle.TotalSeconds * resetCycles)
			{
				return ResetBitR();
			}

			return false;
		}
		private bool ModifyCurrentPage()
		{
			if (random.Next(0, 2) == 0)
				return accessor.ModifyPage(currentPage);

			return false;
		}

		private IModifiablePage FindNextPage(IPage page)
		{
			return list.Pages.SingleOrDefault(x => x.ID == page.Next);
		}
	}
}
