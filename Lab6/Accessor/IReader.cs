﻿using Lab6.Memory.Page;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab6.Accessor
{
	public interface IReader
	{
		bool ReadPage(IReadablePage page);
	}
}
