﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab6.Memory.Page;

namespace Lab6.Accessor
{
    public class PageAccessor : PageModifier, IAccessor
    {
        public bool ResetBitR(IModifiablePage page)
        {
            page.IsReaded = false;

            return true;
        }

        public bool HasNextPage(IPage page)
        {
            return page.Next != Guid.Empty;
        }
    }
}
