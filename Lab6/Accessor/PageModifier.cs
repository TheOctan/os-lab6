﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab6.Memory.Page;

namespace Lab6.Accessor
{
	public class PageModifier : PageReader, IModifier
	{
		public bool ModifyPage(IModifiablePage page)
		{
			ReadPage(page);
			page.IsModified = true;

			return true;
		}
	}
}
